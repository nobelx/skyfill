package pixeltriangle

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2020 Sybren A. Stüvel.
 *
 * This file is part of Skyfill.
 *
 * Skyfill is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skyfill is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Skyfill.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"math"
	"math/rand"

	"gitlab.com/dr.sybren/skyfill/binsearch"
	"gitlab.com/dr.sybren/skyfill/lincolor"
	"gitlab.com/dr.sybren/skyfill/sigmoid"
	"gitlab.com/dr.sybren/skyfill/sky"
)

// PixelTriangle represents a triangle of pixels. It has one pixel at the top,
// and the width of the input image at the bottom.
type PixelTriangle struct {
	pixels [][]lincolor.Pixel
}

// New constructs a pixel triangle suitable for filling up the sky.
func New(sourceImage lincolor.Image, skySize sky.Size) PixelTriangle {
	skyHeight := skySize.TotalHeight()
	pixelTriangle := make([][]lincolor.Pixel, skyHeight)

	imgWidthFloat := float64(sourceImage.Bounds().Dx())
	maxPixelCount := math.Floor(imgWidthFloat / 16)

	scaleFactor := 1.0 / float64(skyHeight)
	scaleFunc := func(line int) float64 {
		skyPerc := float64(line) * scaleFactor
		return math.Sin(skyPerc*math.Pi/2) * maxPixelCount
	}

	var pixels []lincolor.Pixel
	skyBlendSigmoid := sigmoid.NewSigmoid()
	for line := skyHeight - 1; line >= 0; line-- {
		pixelCount := bound(1, scaleFunc(line), maxPixelCount)
		interval := imgWidthFloat / pixelCount

		// Offset for the entire row.
		xOffset := rand.Float64() * interval
		pixels = pixelRow(sourceImage, pixelCount, xOffset, interval, line+1, pixels)

		if line > skySize.PureSkyHeight {
			// Part of extra sky, so blend in with the existing pixels.
			alpha := float64(line-skySize.PureSkyHeight) / float64(skySize.BlendHeight-1)
			alpha = skyBlendSigmoid.Value(alpha)

			extraPixels := pixelRow(sourceImage, pixelCount, xOffset, interval, line+1, nil)

			for idx := range pixels {
				pixels[idx].Color = lincolor.Alpha(pixels[idx].Color, extraPixels[idx].Color, alpha)
			}
		}
		pixelTriangle[line] = pixels
	}

	// Fade the triangle towards the top pixel.
	fadeFactor := 1 / float64(skyHeight)
	fadeToColor := pixelTriangle[0][0].Color
	for line := range pixelTriangle {
		alpha := float64(line) * fadeFactor
		if alpha >= 1.0 {
			break
		}
		alpha = skyBlendSigmoid.Value(alpha)
		for pixelIndex := range pixelTriangle[line] {
			pixelTriangle[line][pixelIndex].Color = lincolor.Alpha(
				fadeToColor,
				pixelTriangle[line][pixelIndex].Color,
				alpha)
		}
	}

	return PixelTriangle{
		pixelTriangle,
	}
}

func pixelRow(
	sourceImage lincolor.Image,
	pixelCount, xOffset, interval float64,
	sourceLine int,
	prevPixels []lincolor.Pixel,
) []lincolor.Pixel {
	pixels := make([]lincolor.Pixel, int(pixelCount))

	windowWidth := int(interval)
	if windowWidth < 3 {
		windowWidth = 3
	}

	var color lincolor.LinearColor
	imageWidth := sourceImage.Bounds().Dx()
	for pixelIndex := 0; pixelIndex < int(pixelCount); pixelIndex++ {
		x := int(float64(pixelIndex)*interval + xOffset)
		pixels[pixelIndex].X = x

		if prevPixels == nil {
			color = sourceImage.AverageColor(sourceLine, x-windowWidth, windowWidth*2+1)
		} else {
			color = binsearch.Interpolate(prevPixels, x, imageWidth)
		}

		pixels[pixelIndex].Color = color
	}

	return pixels
}

func bound(lower, value, higher float64) float64 {
	if value < lower {
		return lower
	}
	if value > higher {
		return higher
	}
	return value
}

func blendPixelRow(
	sourceImage lincolor.Image,
	pixelCount, interval float64,
	sourceLine int,
	pixels []lincolor.Pixel,
	alpha float64,
) {
	windowWidth := int(interval)
	if windowWidth < 1 {
		windowWidth = 1
	}

	var color lincolor.LinearColor
	for index := range pixels {
		color = sourceImage.AverageColor(
			sourceLine,
			pixels[index].X-windowWidth,
			windowWidth*2+1)
		pixels[index].Color = lincolor.Alpha(pixels[index].Color, color, alpha)
	}
}
