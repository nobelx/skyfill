package lincolor

import (
	"image/color"
	"math"
)

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2020 Sybren A. Stüvel.
 *
 * This file is part of Skyfill.
 *
 * Skyfill is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skyfill is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Skyfill.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

// TransferFunctionAdobeRGB converts between linear and AdobeRGB.
type TransferFunctionAdobeRGB struct {
}

// Name returns "AdobeRGB".
func (tf TransferFunctionAdobeRGB) Name() string {
	return "AdobeRGB"
}

// ToLinear converts the color from AdobeRGB to linear.
func (tf TransferFunctionAdobeRGB) ToLinear(c color.Color) LinearColor {
	return simpleToLinear(c, adobeRGB2linear)
}

// FromLinear returns the linear color as AdobeRGB.
func (tf TransferFunctionAdobeRGB) FromLinear(lc LinearColor) color.Color {
	return simpleFromLinear(lc, linear2AdobeRGB)
}

// Converts float [0, 1] to float [0, 1] but with AdobeRGB to linear gamma conversion.
func adobeRGB2linear(argb float64) float64 {
	return math.Pow(argb, 2.19921875)
}

// Converts float [0, 1] to float [0, 1] but with linear to AdobeRGB gamma conversion.
func linear2AdobeRGB(linear float64) float64 {
	return math.Pow(linear, 1/2.19921875)
}
