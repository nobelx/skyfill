package lincolor

import (
	"image/color"
	"math"
)

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2020 Sybren A. Stüvel.
 *
 * This file is part of Skyfill.
 *
 * Skyfill is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skyfill is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Skyfill.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

// TransferFunctionSRGB converts between linear and sRGB.
type TransferFunctionSRGB struct {
}

// Name returns "sRGB".
func (tf TransferFunctionSRGB) Name() string {
	return "sRGB"
}

// ToLinear converts the color from sRGB to linear.
func (tf TransferFunctionSRGB) ToLinear(c color.Color) LinearColor {
	return simpleToLinear(c, srgb2linear)
}

// FromLinear returns the linear color as sRGB.
func (tf TransferFunctionSRGB) FromLinear(lc LinearColor) color.Color {
	return simpleFromLinear(lc, linear2srgb)
}

// Converts float [0, 1] to float [0, 1] but with linear to sRGB gamma conversion.
func linear2srgb(linear float64) float64 {
	if linear <= 0.0031308 {
		return 12.92 * linear
	}
	return 1.055*math.Pow(linear, 1/2.4) - 0.055
}

// Converts float [0, 1] to float [0, 1] but with sRGB to linear gamma conversion.
func srgb2linear(srgb float64) float64 {
	if srgb < 0.0404482362771082 {
		return srgb / 12.92
	}
	return math.Pow((srgb+0.055)/1.055, 2.4)
}
