package sky

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2020 Sybren A. Stüvel.
 *
 * This file is part of Skyfill.
 *
 * Skyfill is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skyfill is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Skyfill.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"image"
	"image/draw"

	"gitlab.com/dr.sybren/skyfill/lincolor"
	"gitlab.com/dr.sybren/skyfill/sigmoid"
)

// Blend merges the sky from the sky image into the target image.
func Blend(targetImage lincolor.Image, skyImage lincolor.Image, skySize Size) {
	// Blend the blend area into the image.
	sigmoid := sigmoid.NewSigmoid()
	imgBounds := targetImage.Bounds()
	blendFactor := 1.0 / float64(skySize.BlendHeight)
	for offset := 0; offset < skySize.BlendHeight; offset++ {
		level := 1 - float64(offset)*blendFactor
		blendAlpha := sigmoid.Value(level)

		y := offset + skySize.PureSkyHeight
		for x := imgBounds.Min.X; x < imgBounds.Max.X; x++ {
			origColor := targetImage.LinearAt(x, y)
			skyColor := skyImage.LinearAt(x, y)
			blended := lincolor.Alpha(origColor, skyColor, blendAlpha)
			targetImage.LinearSet(x, y, blended)
		}
	}

	// Draw the pure sky part.
	draw.Draw(targetImage, skySize.PureSkyBounds(), skyImage, image.ZP, draw.Src)
}
