module gitlab.com/dr.sybren/skyfill

require (
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/sirupsen/logrus v1.3.0
	github.com/stretchr/testify v1.3.0
	golang.org/x/crypto v0.0.0-20190225124518-7f87c0fbb88b // indirect
	golang.org/x/image v0.0.0-20190227222117-0694c2d4d067
	golang.org/x/sys v0.0.0-20190225065934-cc5685c2db12 // indirect
	gopkg.in/check.v1 v1.0.0-20200902074654-038fdea0a05b // indirect
	gopkg.in/mgo.v2 v2.0.0-20180705113604-9856a29383ce
	gopkg.in/yaml.v2 v2.3.0 // indirect
)

go 1.13
