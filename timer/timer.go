package timer

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2020 Sybren A. Stüvel.
 *
 * This file is part of Skyfill.
 *
 * Skyfill is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skyfill is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Skyfill.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"time"

	"github.com/sirupsen/logrus"
)

// DurationTracker tracks the duration from construction to each Duration call.
type DurationTracker struct {
	startTime time.Time
}

// NewDurationTracker constructs a DurationTracker and starts its timer.
func NewDurationTracker() DurationTracker {
	return DurationTracker{time.Now()}
}

// Duration returns the duration since this tracker was constructed.
func (dt DurationTracker) Duration() time.Duration {
	return time.Now().Sub(dt.startTime)
}

// Logger returns a top-level Logrus logger with the given field set to the tracked duration.
func (dt DurationTracker) Logger(fieldname string) *logrus.Entry {
	return logrus.WithField(fieldname, dt.Duration())
}

// LogField returns the logger with the given field set to the tracked duration.
func (dt DurationTracker) LogField(logger *logrus.Entry, fieldname string) *logrus.Entry {
	return logger.WithField(fieldname, dt.Duration())
}
