package main

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2019 Sybren A. Stüvel.
 *
 * This file is part of Skyfill.
 *
 * Skyfill is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skyfill is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Skyfill.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"flag"
	"fmt"
	"image"
	"image/color"
	"log"
	"os"
	"path"
	"strings"

	"github.com/sirupsen/logrus"
	"gitlab.com/dr.sybren/skyfill/imgio"
	"gitlab.com/dr.sybren/skyfill/lincolor"
	"gitlab.com/dr.sybren/skyfill/pixeltriangle"
	"gitlab.com/dr.sybren/skyfill/sky"
	"gitlab.com/dr.sybren/skyfill/timer"
)

const applicationName = "Skyfill"

var applicationVersion = "set-during-build"

var cliArgs struct {
	filename        string
	quiet           bool
	debug           bool
	blendArea       int
	blendAreaFactor float64
	skyHeight       int
	lowerSky        int
	lowerSkyFactor  float64
	showSamples     bool
	showBounds      bool
	adobeRGB        bool /* Assume AdobeRGB instead of sRGB. */
}

func parseCliArgs() {
	flag.BoolVar(&cliArgs.quiet, "quiet", false, "Disable info-level logging.")
	flag.BoolVar(&cliArgs.debug, "debug", false, "Enable debug-level logging.")
	flag.BoolVar(&cliArgs.showSamples, "samples", false, "Show sample points as red pixels.")
	flag.BoolVar(&cliArgs.showBounds, "bounds", false, "Show sky gap bounds.")
	flag.IntVar(&cliArgs.skyHeight, "sky", 0, "Height of the sky gap in pixels. Overrides automatic sky gap detection.")
	flag.IntVar(&cliArgs.lowerSky, "lower-sky", 0, "Amount to lower the sky boundary by, in percent of detected sky gap height.")
	flag.IntVar(&cliArgs.blendArea, "blend", 10, "Height of the blend area, in percent of detected sky gap height. Prevents a hard edge between the generated and the real sky.")
	flag.BoolVar(&cliArgs.adobeRGB, "adobergb", false, "Assume the input image is AdobeRGB. Without this option, sRGB is assumed.")
	flag.Parse()

	if flag.NArg() != 1 {
		flag.Usage()
		fmt.Println("  filename")
		fmt.Println("        Input file; PNG or TIFF file in 8- or 16-bit RGB or RGBA mode")
		os.Exit(1)
	}

	cliArgs.filename = flag.Arg(0)
	if cliArgs.filename == "" {
		logrus.Fatal("empty filename not allowed")
	}
	cliArgs.blendAreaFactor = float64(cliArgs.blendArea) / 100.0
	cliArgs.lowerSkyFactor = float64(cliArgs.lowerSky) / 100.0
}

func configLogging() {
	logrus.SetFormatter(&logrus.TextFormatter{
		FullTimestamp: true,
	})

	// Only log the warning severity or above by default.
	var level logrus.Level
	switch {
	case cliArgs.debug:
		level = logrus.DebugLevel
	case cliArgs.quiet:
		level = logrus.WarnLevel
	default:
		level = logrus.InfoLevel
	}
	logrus.SetLevel(level)
	log.SetOutput(logrus.StandardLogger().Writer())
}

func determineSky(image image.Image) (skySize sky.Size) {
	var logMessage string

	if cliArgs.skyHeight > 0 {
		skySize = sky.NewSize(image.Bounds(), cliArgs.skyHeight, cliArgs.blendAreaFactor)
		logMessage = "sky height given on CLI"
	} else {
		skySize = sky.FindSkyHeight(image, cliArgs.blendAreaFactor, cliArgs.lowerSkyFactor)
		logMessage = "detected sky"
	}

	logrus.WithFields(logrus.Fields{
		"pureSkyHeight":   skySize.PureSkyHeight,
		"blendAreaPixels": skySize.BlendHeight,
	}).Info(logMessage)

	if skySize.IsEmpty() {
		logrus.Fatal("no sky detected, nothing to fill")
	}

	return
}

func findTransferFunction(sourceImage image.Image) lincolor.TransferFunction {
	// TODO(Sybren): Get this from the image metadata rather than requiring CLI arguments.
	if cliArgs.adobeRGB {
		return lincolor.TransferFunctionAdobeRGB{}
	}
	return lincolor.TransferFunctionSRGB{}
}

func getOutputFilename(outputImage image.Image, inputFilename string) string {
	extension := strings.ToLower(path.Ext(inputFilename))
	withoutExtension := inputFilename[:len(inputFilename)-len(extension)]

	var outputExtension string
	switch outputImage.ColorModel() {
	case color.NRGBA64Model, color.RGBA64Model:
		logrus.Info("input was more than 8 bit per color, writing as TIFF")
		outputExtension = ".tif"
	default:
		outputExtension = ".jpg"
	}

	return withoutExtension + "-filled" + outputExtension
}

func main() {
	totalTracker := timer.NewDurationTracker()

	parseCliArgs()
	configLogging()
	logrus.WithField("version", applicationVersion).Infof("starting %s", applicationName)

	inputImage := imgio.LoadImage(cliArgs.filename)
	tf := findTransferFunction(inputImage)
	logrus.WithFields(logrus.Fields{
		"colorSpace": tf.Name(),
		"colorModel": fmt.Sprintf("%T", inputImage.ColorModel().Convert(color.RGBA{})),
	}).Info("color info")

	sourceImage := lincolor.WrapImage(inputImage, tf)
	skySize := determineSky(sourceImage)
	skyImage := lincolor.WrapImage(
		imgio.NewImage(sourceImage.ColorModel(), skySize.TotalBounds()),
		tf)

	logrus.Info("filling sky")
	fillTracker := timer.NewDurationTracker()
	pixelTriangle := pixeltriangle.New(sourceImage, skySize)
	pixelTriangle.Expand(skyImage)

	fillTracker.Logger("fillDuration").Info("blending sky")
	blendTracker := timer.NewDurationTracker()
	sky.Blend(sourceImage, skyImage, skySize)

	if cliArgs.showSamples {
		pixelTriangle.DrawSamples(sourceImage)
	}

	if cliArgs.showBounds {
		skySize.DrawBounds(sourceImage)
	}

	blendTracker.Logger("blendDuration").Info("writing image")
	outfname := getOutputFilename(sourceImage, cliArgs.filename)
	imgio.WriteImage(outfname, sourceImage)

	totalTracker.Logger("totalDuration").Info("done")
}
