# Skyfill Changelog

## Version 1.3 (2020-10-13)

- Fixed bug where some 16-bit files would be written without file extension.


## Version 1.2 (2020-09-13)

- Fixed bug where too much of the sky was filled up.
- Add CLI option `-adobergb` to indicate the input should be interpreted as
  AdobeRGB. Otherwise sRGB is assumed.
- Write 16-bit TIFF when the input is 16-bit.


## Version 1.1 (2019-05-06)

- Assume full white is also not part of the sky and should be filled.
- Save to both PNG and JPEG.


## Version 1.0 (2019-03-17)

- First released version
