package imgio

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2019 Sybren A. Stüvel.
 *
 * This file is part of Skyfill.
 *
 * Skyfill is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skyfill is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Skyfill.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"fmt"
	"image"
	"image/color"
	"os"
	"path"
	"strings"

	// Import for side-effect of registering decoder.
	_ "image/jpeg"
	_ "image/png"

	"github.com/sirupsen/logrus"
	"gitlab.com/dr.sybren/skyfill/timer"
	"golang.org/x/image/tiff"
)

var extIsTiff = map[string]bool{
	".tif":  true,
	".tiff": true,
}

// Mutable images can have their pixel set to a certain colour.
type Mutable interface {
	image.Image
	Set(x, y int, c color.Color)
}

// LoadImage loads an image, converting it to RGBA if necessary.
func LoadImage(filename string) Mutable {
	logger := logrus.WithField("filename", filename)
	logger.Info("loading image")
	tracker := timer.NewDurationTracker()

	reader, err := os.Open(filename)
	if err != nil {
		logger.WithError(err).Fatal("unable to open file")
	}
	defer reader.Close()

	var img image.Image
	ext := strings.ToLower(path.Ext(filename))
	if extIsTiff[ext] {
		logger.Debug("loading TIFF image")
		img, err = tiff.Decode(reader)
	} else {
		logger.Debug("loading generic image")
		img, _, err = image.Decode(reader)
	}

	if err != nil {
		logger.WithError(err).Fatal("unable to decode file")
	}

	imgBounds := img.Bounds()
	logger = logger.WithFields(logrus.Fields{
		"imageWidth":  imgBounds.Dx(),
		"imageHeight": imgBounds.Dy(),
	})
	tracker.LogField(logger, "loadDuration").Info("loaded image")

	if mutableImage, ok := img.(Mutable); ok {
		return mutableImage
	}

	logger.WithField("imageType", fmt.Sprintf("%T", img)).Warning("image needs in-memory conversion; use 8-bit TIFF or PNG to prevent this")
	tracker = timer.NewDurationTracker()

	bounds := img.Bounds()
	convertedImage := image.NewNRGBA(bounds)

	for y := bounds.Min.Y; y < bounds.Max.Y; y++ {
		for x := bounds.Min.X; x < bounds.Max.X; x++ {
			convertedImage.Set(x, y, img.At(x, y))
		}
	}

	tracker.LogField(logger, "convertDuration").Info("conversion done")
	return convertedImage
}
