package imgio

/* ***** BEGIN GPL LICENSE BLOCK *****
 *
 * Copyright (C) 2019 Sybren A. Stüvel.
 *
 * This file is part of Skyfill.
 *
 * Skyfill is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Skyfill is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Skyfill.  If not, see <https://www.gnu.org/licenses/>.
 *
 * ***** END GPL LICENSE BLOCK ***** */

import (
	"image"
	"image/jpeg"
	"image/png"
	"os"
	"path"
	"strings"

	"github.com/sirupsen/logrus"
	"gitlab.com/dr.sybren/skyfill/lincolor"
	"gitlab.com/dr.sybren/skyfill/timer"
	"golang.org/x/image/tiff"
)

// WriteImage writes to a PNG file.
func WriteImage(fname string, img image.Image) {
	ext := path.Ext(fname)

	// Convert a wrapped image to its wrapped type, so that the encoder can detect
	// the proper bit depth from it.
	if linearImage, ok := img.(lincolor.Image); ok {
		img = linearImage.Wrapped()
	}

	logger := logrus.WithField("file", fname)
	logger.Debug("writing image")
	tracker := timer.NewDurationTracker()

	writer, err := os.Create(fname)
	if err != nil {
		logger.WithError(err).Fatal("unable to open file")
	}
	defer writer.Close()

	switch strings.ToLower(ext) {
	case ".png":
		err = png.Encode(writer, img)
	case ".jpg", ".jpeg":
		err = jpeg.Encode(writer, img, &jpeg.Options{Quality: 90})
	case ".tif", ".tiff":
		err = tiff.Encode(writer, img, &tiff.Options{
			Compression: tiff.Deflate,
			Predictor:   true,
		})
	default:
		logger.WithField("ext", ext).Fatal("unknown extension, unable to write image")
	}
	if err != nil {
		logger.WithError(err).Fatal("unable to write image")
	}

	tracker.LogField(logger, "writeDuration").Info("image written")
}
